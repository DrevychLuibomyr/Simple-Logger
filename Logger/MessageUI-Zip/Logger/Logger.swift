//
//  Logger.swift
//  MessageUI-Zip
//
//  Created by Luybckyk Drevych on 6/21/19.
//  Copyright © 2019 Luybckyk Drevych. All rights reserved.
//

import UIKit
import SSZipArchive

protocol LogCreator {
    func log() -> String
}

public enum LogLevel: String {
    case error = "[‼️]" // error
    case info = "[ℹ️]" // info
    case debug = "[💬]" // debug
    case verbose = "[🔬]" // verbose
    case warning = "[⚠️]" // warning
    case severe = "[🔥]" // severe
    case leave = "[🏃🏻‍♂️]" //leave
    case enter = "[🧗‍♂️]" //enter
    
    var description: String {
        return self.rawValue
    }
}

final class Logger {
    
    static let sharedInstance = Logger()
    private init() {}
    
    static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter
    }
    
    public func tempZipPath() -> String {
        let filename = "App_logs.zip"
        let path = "\(NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0])/\(filename)"
        return path
    }
    
    public func log(_ event: LogLevel, file: String = #file, line: Int = #line, column: Int = #column, functionName: String = #function, object: Any, message: LogCreator) {
        switch event {
        case let x:
            let log = "\n\(Date().toString()) \(x.description) [\(Logger.sourceFileName(filePath: file))]\n Line: \(line)\n Column: \(column)\n Function Name: \(functionName)\n Object: \(object)\n Message: \(message.log())\n"
            print(log)
            self.write(log)
        }
    }
    
    public func removeZipFromCache() {
        let dirPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent("App_logs.zip")
        do {
            if FileManager.default.fileExists(atPath: dirPath.path) {
                try FileManager.default.removeItem(at: dirPath)
                clearLogsDir()
            } else {
                print("File don't exists")
            }
        } catch let error {
            print("An error took place: \(error.localizedDescription)")
        }
    }
    
    public func deviceInfo() -> String {
        let device = UIDevice.current
        var info = "Device info\n"
        info += "Name: \(device.name)\n"
        info += "System Name: \(device.systemName)\n"
        info += "System Version: \(device.systemVersion)\n"
        info += "Model Name: \(device.modelName)\n"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String { info += "App Version: \(version)\n" }
        if let buildNumber = Bundle.main.infoDictionary!["CFBundleVersion"] as? String {
            info += "Build Number: \(buildNumber)\n"
        }
        info += "Localized Model: \(device.localizedModel)\n"
        info += "Identifier For Vendor: \((device.identifierForVendor?.uuidString)!)\n"
        info += "Mult tasking support: \(device.isMultitaskingSupported.description)\n"
        
        return info
    }
    
    public func logActivityItems(_ completion: @escaping ([UIActivityItemProvider]?) -> Void) {
        DispatchQueue.main.async { [weak self] in
            guard let items = try? self?.logActivityItems() else {
                completion(nil)
                return
            }
            
            completion(items)
        }
    }
    
    //MARK: Private
    private func write(_ string: String) {
        let log = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(Date().toString()).txt")
        if let handler = try? FileHandle(forWritingTo: log)  {
            handler.seekToEndOfFile()
            handler.write(string.data(using: .utf8)!)
            handler.closeFile()
        } else {
            try? string.data(using: .utf8)?.write(to: log)
        }
    }
    
    private func logActivityItems() throws -> [UIActivityItemProvider] {
        let log = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].path
        let pathToZip = Logger.sharedInstance.tempZipPath()
        _ = SSZipArchive.createZipFile(atPath: pathToZip, withContentsOfDirectory: log)
        
        let deviceInfoString = deviceInfo()
        let logItem = MailItemProvider(subject: "MessageUI_Log " + Date().toString(), body: deviceInfoString)
        let atachment = MailItemAttachmentProvider(url: URL(fileURLWithPath: self.tempZipPath()))
        return [logItem, atachment]
    }
    
    
    private func clearLogsDir() {
        let logDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        do {
            let dirContent = try FileManager.default.contentsOfDirectory(atPath: logDir)
            guard dirContent.count > 6 else { return }
            for content in dirContent {
                try FileManager.default.removeItem(atPath: logDir + "/\(content)")
            }
        } catch {
            print("Could not clean folder with error: \(error.localizedDescription)")
        }
    }
    
    private class func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last!
    }
    
}

//MARK: Date+toString
internal extension Date {
    func toString() -> String {
        return Logger.dateFormatter.string(from: self as Date)
    }
}
