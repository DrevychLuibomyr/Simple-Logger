//
//  AppDelegate.swift
//  MessageUI-Zip
//
//  Created by Luybckyk Drevych on 6/20/19.
//  Copyright © 2019 Luybckyk Drevych. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Logger.sharedInstance.log(.enter, object: self, message: self)
        return true
    }
}

extension AppDelegate: LogCreator {
    func log() -> String {
        return "AppDelegate -> Start"
    }
}

