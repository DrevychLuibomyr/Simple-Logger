//
//  MailItemAttachmentProvider.swift
//  MessageUI-Zip
//
//  Created by Luybckyk Drevych on 7/8/19.
//  Copyright © 2019 Luybckyk Drevych. All rights reserved.
//

import UIKit
import Foundation

final class MailItemAttachmentProvider: UIActivityItemProvider {
    
    var url: Foundation.URL
    
    init(url: Foundation.URL) {
        self.url = url
        super.init(placeholderItem: url)
    }
    
    override var item : Any {
        return url as AnyObject
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "App_logs.zip"
    }
    
}
