//
//  MailItemProvider.swift
//  MessageUI-Zip
//
//  Created by Luybckyk Drevych on 7/8/19.
//  Copyright © 2019 Luybckyk Drevych. All rights reserved.
//

import UIKit
import Foundation

final class MailItemProvider: UIActivityItemProvider {
    
    var subject: String
    var body: String
    
    init(subject: String, body: String) {
        self.subject = subject
        self.body = body
        super.init(placeholderItem: body)
    }
    
    override var item : Any {
        return self.body as AnyObject
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return subject
    }
    
}


