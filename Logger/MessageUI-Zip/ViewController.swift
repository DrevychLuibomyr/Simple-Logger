//
//  ViewController.swift
//  MessageUI-Zip
//
//  Created by Luybckyk Drevych on 6/20/19.
//  Copyright © 2019 Luybckyk Drevych. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapOnSettings(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Share Logs", message: "", preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Send Logs", style: .default) { [unowned self] action in
            self.sendMail(sender: sender)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(action)
        alertController.addAction(cancelAction)
        
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    private func sendMail(sender: UIButton) {
        Logger.sharedInstance.removeZipFromCache()
        Logger.sharedInstance.logActivityItems { [weak self] (items) in
            guard let items = items else {
                return
            }
            let share = UIActivityViewController(activityItems: items, applicationActivities: nil)
            share.popoverPresentationController?.sourceView = sender
            share.popoverPresentationController?.sourceRect = sender.bounds
            self?.present(share, animated: true, completion: nil)
        }
    }
}

extension ViewController: LogCreator {
    func log() -> String {
        return "ViewController -> Start"
    }
}

